import math

from bokeh.models import ColumnDataSource
from bokeh.models.ranges import FactorRange
from bokeh.plotting import Figure


class SDRFigure:
    def __init__(
        self,
        *args,
        x_range=("1", "2", "3"),
        y_range=("1", "2", "3"),
        title="SDR",
        x_axis_location="above",
        tools="hover",
        tooltips=[("coordinate", "@row_name, @col_name"), ("count", "@counts")],
        toolbar_location=None,
        width=450,
        height=450,
        **kwargs
    ):
        default_data_dict = dict(row_name=[], col_name=[], alphas=[], counts=[])
        self.figure = Figure(
            *args,
            title=title,
            x_axis_location=x_axis_location,
            tools=tools,
            tooltips=tooltips,
            toolbar_location=toolbar_location,
            width=width,
            height=height,
            x_range=x_range,
            y_range=y_range,
            **kwargs
        )
        self._sdr_data_source = ColumnDataSource(data=default_data_dict)
        self._width = 1

        self.figure.axis.major_label_text_font_size = "0pt"
        self.figure.axis.major_tick_line_color = None
        self.figure.grid.grid_line_color = None

        self.figure.rect(
            "row_name",
            "col_name",
            0.9,
            0.9,
            source=self._sdr_data_source,
            alpha="alphas",
        )

    def update_with_sdr(self, sdr, total_bits, reduction_factor=1):
        sdr_dict, self._width = self._sdr_to_count_dict(
            sdr, total_bits, reduction_factor
        )
        self._sdr_data_source.data = sdr_dict
        self.figure.x_range = FactorRange(factors=[str(i) for i in range(self._width)])
        self.figure.y_range = FactorRange(
            factors=[str(i) for i in range(self._width - 1, -1, -1)]
        )
        # TODO: update based on new `width`?

    @staticmethod
    def _sdr_to_count_dict(sdr, total_bits, reduction_factor):
        rows = []
        columns = []
        counts = []

        counts_by_slots = [
            sum(
                [
                    1
                    for j in range(
                        reduction_factor * i, reduction_factor * i + reduction_factor
                    )
                    if j in sdr
                ]
            )
            for i in range(
                int(total_bits / reduction_factor)
                + (1 if total_bits % reduction_factor != 0 else 0)
            )
        ]
        width = round(math.sqrt(len(counts_by_slots)))
        max_count = 0
        for i, count in enumerate(counts_by_slots):
            r = int(i / width)
            c = i % width
            max_count = max(max_count, count)

            rows.append(r)
            columns.append(c)
            counts.append(count)

        alphas = [0.1 + (0.9 * count / max_count) for count in counts]

        data = dict(
            row_name=[str(t) for t in columns],
            col_name=[str(t) for t in rows],
            # colors=color,
            alphas=alphas,
            counts=counts,
        )
        return data, width


if __name__ == "__main__":
    from bokeh.layouts import layout
    from bokeh.plotting import output_file, show

    output_file("explore.html")

    from zhtm.encoders.scalar_encoder import ScalarEncoder

    e = ScalarEncoder(num_buckets=256)

    input_sdr_fig = SDRFigure(
        title="Input SDR",
        width=350,
        height=350,
    )
    input_sdr_fig.update_with_sdr(e.encode(32), e.num_bits, 4)

    f = Figure(title="Ahh!", width=300, height=300)
    f.line([1, 2, 3, 4], [2, 4, 2, 4])

    l = layout([f], [input_sdr_fig.figure])
    show(l)
