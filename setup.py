from distutils.core import setup

setup(
    name="zhtm_bokeh",
    version="0.1dev",
    author="Zachary Cross",
    author_email="zachary.t.cross@gmail.com",
    description="Bokeh helpers for zhtm",
    url="https://bitbucket.org/zhtm/zhtm_bokeh/src/master/",
    packages=["zhtm_bokeh"],
    license="GNU Affero General Public License v3 or later (AGPLv3+)",
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
    ],
    install_requires=["bokeh"],
)
